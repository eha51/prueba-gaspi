import React, {useContext} from 'react';
import AppBar from '@mui/material/AppBar';
import Box from '@mui/material/Box';
import Toolbar from '@mui/material/Toolbar';
import Typography from '@mui/material/Typography';
import Button from '@mui/material/Button';
import { env } from '../environment/environment';
import { useNavigate } from 'react-router-dom';
import { useDispatch, useSelector } from 'react-redux';
import { resetProductsAction, setListProductsAction, setSelectProductAction } from '../redux/actions/productsAction';
import { MainContext } from '../contexts/MainContext';
import { Droppable } from 'react-drag-and-drop'

export default function Navbar() {

  const navigate = useNavigate()
  const dispatch = useDispatch();
  const {handleSetIsCar} = useContext(MainContext)
  const {list,selectProducts} = useSelector(state => state.products)

  const onDrop = (data) => {
    const select = list.find((item) => item.uuid == data?.product);
    const newList = list.filter((item) => {
      return item.uuid !== select?.uuid
    });
    dispatch(setListProductsAction(newList));
    dispatch(setSelectProductAction([...selectProducts,select]))
  }

  return (
    <Box sx={{ flexGrow: 1 }}>
      <AppBar position="fixed" color={'primary'}>
        <Toolbar>
          <article style={{backgroundColor:"white"}} onClick={() => {
            navigate('/')
          }}>
            <img src={env.imgGaspi} />
          </article>
          <Typography variant="h6" component="div" sx={{ flexGrow: 1 }}>
            e-Commerce Gapsi 
          </Typography>
          <Droppable
            types={['product']} // <= allowed drop types
            onDrop={onDrop.bind(this)}>
                <Button color="inherit" onClick={() => {
                  handleSetIsCar(true);
                }}>Carrito {selectProducts && selectProducts.length > 0 ? ` (${selectProducts.length})`:''}</Button>
          </Droppable>
          <Button color="inherit" onClick={() => {
            dispatch(resetProductsAction());
            navigate('/')
          }}>Reiniciar</Button>
        </Toolbar>
      </AppBar>
    </Box>
  );
}
