import * as React from 'react';
import Box from '@mui/material/Box';
import TextField from '@mui/material/TextField';

export default function Search({onSearch=() => {}}) {
  return (
    <Box
      sx={{
        width: '100%',
      }}
    >
      <TextField fullWidth label="¿Que esta buscando?" id="fullWidth" onKeyDown={onSearch} />
    </Box>
  );
}
