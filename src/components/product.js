import React from "react";
import { Draggable } from 'react-drag-and-drop'

const Product = React.memo(({
  product
}) => {


  return (
    <Draggable type="product" data={product.uuid}>
      <article className="product-container">
        <div className="productimg">
          <img src={product?.image} alt={product?.title} width="80px"/>
        </div>
        <h1 className="producttitle">{product?.title}</h1>
        <p className="productprice">{product?.price}</p> 
      </article>
    </Draggable>
  );

});

export default Product;