import { typesRequestErrors } from "../../constants/errorConstant";
import { types } from "../types/types";

const initialState = {
  list: [],
  selectProducts: [],
  isLoading: false,
  errors: {
    [typesRequestErrors.getListProducts]: {
      isError: false,
      msg: null
    }
  }
};

export const productsReducer = (state = initialState, action = {}) => {
  switch(action.type) {
    case types.productsSetListProducts:
      return {
        ...state,
        list: action.payload.products,
      }
    case types.productsSetLoading:
      return {
        ...state,
        isLoading: action.payload.isLoading,
      }
    case types.productsSetSelectProduct:
      return {
        ...state,
        selectProducts: action.payload.products
      }
    case types.productsErrors:
      return productsTypeErrors(state,action)
    case types.productsReset:
      return {
        ...initialState
      }
    default:
      return state;
  }
};

const productsTypeErrors = (state,action) => {
  switch(action.typeError) {
    case typesRequestErrors.getListProducts:
      return {
        ...state,
        errors: {
          ...state.errors,
          [typesRequestErrors.getListProducts]: {
            isError: action.payload.isError,
            msg: action.payload.msg
          }
        }
      }
    default:
      return state;
  }
}