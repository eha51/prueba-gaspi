import axios from "axios";
import { env } from "../../environment/environment";
import { convertCurrency } from "../../helpers/currency";
import { handleError } from "../../helpers/handleError"


export const getProductsService = async(query,page) => {
  try {
    const url = env.getProducts;

    const headers = {
      'X-IBM-Client-Id': env.token
    }

    const params = {
      query,
      page
    }

    const {data:res} = await axios({
      method: 'get',
      url,
      headers,
      params
    })

    const { items } = res?.item?.props?.pageProps?.initialData?.searchResult?.itemStacks[0];

    const products = items.map((item,index) => {
      return {
        uuid: (new Date()).getTime() + index,
        image: item?.image,
        title: item?.name,
        price: convertCurrency(item?.price.toString()),
        rating: {
          ...item.rating
        }
      }
    })

    return products

  } catch (error) {
    const { status,data } = error.response || { status: null, data:null };
    throw new Error(handleError(status,data))
  }
}