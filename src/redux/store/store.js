import { applyMiddleware, combineReducers, compose, legacy_createStore as createStore } from "redux";
import thunk from "redux-thunk";
import { productsReducer } from "../reducers/productsReducers";


const composeEnhancers = (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION_COMPOSE__) || compose;

const reducers = combineReducers({
  products: productsReducer,
})

export const store = createStore(
  reducers,
  composeEnhancers(
    applyMiddleware(thunk) 
  )
);