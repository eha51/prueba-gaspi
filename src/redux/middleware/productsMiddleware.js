import { typesRequestErrors } from "../../constants/errorConstant"
import { setErrorsProductsAction, setListProductsAction, setLoadingProductAction } from "../actions/productsAction"
import { getProductsService } from "../services/productsService"


export const getProductsMiddleware = (search,page) => {
  return (dispatch) => {
    dispatch(setLoadingProductAction(true));
    getProductsService(search,page)
    .then((products) => {
      dispatch(setListProductsAction(products));
    })
    .catch((error) => {
      dispatch(setErrorsProductsAction(
        typesRequestErrors.getListProducts,
        true,
        error
      ))
    })
    .finally(() => {
      dispatch(setLoadingProductAction(false));
    })
  }
}