import { types } from "../types/types"

export const setListProductsAction = (products) => {
  return {
    type: types.productsSetListProducts,
    payload: {
      products
    }
  }
}

export const setErrorsProductsAction = (typeError,isError,msg) => {
  return {
    type: types.productsErrors,
    typeError,
    payload: {
      isError,
      msg
    }
  }
}

export const setSelectProductAction = (products) => {
  return {
    type: types.productsSetSelectProduct,
    payload: {
      products
    }
  }
}

export const setLoadingProductAction = (isLoading) => {
  return {
    type: types.productsSetLoading,
    payload: {
      isLoading
    }
  }
}

export const resetProductsAction = () => {
  return {
    type: types.productsReset,
    payload: {}
  }
}