export const types = {
  productsSetListProducts: '[Products] Set List Products',
  productsSetSelectProduct: '[Products] Set Select Product',
  productsSetLoading: '[Products] Set Loading',
  productsErrors: '[Products] Errors',
  productsReset: '[Products]: Reset',
}