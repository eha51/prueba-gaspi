import React, { useContext, useEffect, useRef, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import Product from "../components/product";
import ProductSkeleton from "../components/productsSkeleton";
import { MainContext } from "../contexts/MainContext";
import { setListProductsAction } from "../redux/actions/productsAction";

const Products = () => {
  const { list,isLoading } = useSelector(state => state.products);
  const [listProducts,setListProducts] = useState([]);
  const { search } = useContext(MainContext);
  const listInnerRef = useRef();


  useEffect(() => {
    setListProducts(list || []);
  },[list])

  const handleList = () => {
    return listProducts && listProducts.length > 0 ? (
      <main 
        className="products-container" 
        ref={listInnerRef}>
         <section className="productsitems">
           {
             listProducts.map((product,index) => {
               return <Product 
                     key={index} 
                     product={product}
                   />;
             })
           }
         </section>
      </main>
     ):'No existen Resultados'
  }

  const handleSkeleton = () => {
    return (
      <div className="skeletonlist">
        <ProductSkeleton />
      </div>
    )
  }

  return (
    <>
      {
        isLoading ? handleSkeleton(): handleList()
      }
    </>
  )
}

export default Products;