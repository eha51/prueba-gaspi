import React, { useContext, useEffect } from 'react';
import Button from '@mui/material/Button';
import { useSelector } from 'react-redux';
import Product from '../components/product';
import { MainContext } from '../contexts/MainContext';

const Car = ({isShow=false}) => {

  const {selectProducts} = useSelector(state => state.products);
  const {handleSetIsCar} = useContext(MainContext);

  useEffect(() => {
    if(isShow) {
      document.body.style.overflowY = "hidden";
    }
    else {
      document.body.style.overflowY = "auto";
    }
  },[isShow])

  return (
    <>
      {
        isShow ? (
          <section className="car-container">
            <div className="carcontent">
              <article className="carclose">
                <Button variant="contained" color="primary" onClick={() => {
                  handleSetIsCar(false)
                }}>Cerrar</Button>
              </article>
              <p className="carcontenttitle">Articulos selecionado</p>
              {
                selectProducts && selectProducts.length > 0 ? (
                  selectProducts.map((product,index) => {
                    return (
                      <Product key={index} product={product}/>
                    )
                  })
                ): ''
              }
            </div>
          </section>
        ): ""
      }
    </>
  )
}

export default Car