import React, { useContext } from 'react'
import { useDispatch } from 'react-redux'
import { useNavigate} from 'react-router-dom'
import Search from '../components/search'
import { MainContext } from '../contexts/MainContext'
import { getProductsMiddleware } from '../redux/middleware/productsMiddleware'

const Home = () => {

  const dispatch = useDispatch();
  const naviagte = useNavigate();
  const { handleSetSearch} = useContext(MainContext)

  const onSearch = (e) => {
    if(e.keyCode === 13) {
      const value = e.target.value.trim()
      handleSetSearch(value,1);
      dispatch(getProductsMiddleware(value,1))
      naviagte('./products');
    }
  }

  return (
    <section className="home-container">
      <article className="homesearch">
        <p className="homesearchtitle">e-Commerce Gaspi</p>
        <Search 
          onSearch={onSearch}
        />
      </article>
    </section>
  )
}

export default Home
