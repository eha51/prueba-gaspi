import React from "react"
import {
  BrowserRouter as Router,
  Routes,
  Route
} from 'react-router-dom'
import Home from "../pages/Home";
import Products from "../pages/Products";
import ButtonAppBar from '../components/navbar';

const AppRoutes = () => {
  return (
    <Router>
      <ButtonAppBar />
      <div className="app-container">
        <Routes>
          <Route 
            path="/products"
            element={<Products />}
          />
          <Route 
            path="/"
            element={<Home />}
          />
          <Route 
            path="*"
            element={<Home />}
          />
        </Routes>
      </div>
    </Router>
  )
}

export default AppRoutes;