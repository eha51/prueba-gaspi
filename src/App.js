import './App.scss';
import { Provider} from 'react-redux';
import { store } from './redux/store/store';
import AppRoutes from './routes/AppRoutes';
import { MainContext } from './contexts/MainContext';
import { useState } from 'react';
import Car from './pages/Car';

function App() {

  const [search,setSearch] = useState({
    value: '',
    page: 0
  });

  const [isCar,setIsCar] = useState(false);

  const handleSetSearch = (value,page) => {
    setSearch({
      value,
      page
    });
  }

  const handleSetIsCar = (isValue) => {
    setIsCar(isValue);
  }


  return (
    <>
      <Provider store={store}>
          <MainContext.Provider value={{
            search,
            handleSetSearch,
            isCar,
            handleSetIsCar
          }}>
            <AppRoutes />
            <Car isShow={isCar} />
          </MainContext.Provider>
      </Provider>
    </>
  );
}

export default App;
