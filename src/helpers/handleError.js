
export const handleError = (status,response) => {

  switch(status) {
    case 401:
      return;
    default:
      if(status && status >= 400) {
        return (response.error && typeof response.status === 'object') ? response.status.info:'Ha ocurrido un error, inténtalo más tarde';
      }
      return 'Ha ocurrido un error, inténtalo más tarde';
  }
}